import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={'https://wisetree.s3.sa-east-1.amazonaws.com/logoWiseTree.png'} className="App-logo" alt="logo" />
        <ul>
        <li><a className="App-link" href="#" target="_blank" rel="noopener noreferrer" > Wise Tree </a></li>
        <li><a className="App-link" href="#" target="_blank" rel="noopener noreferrer" > Buscar </a> </li>
        <li><a className="App-link" href="#" target="_blank" rel="noopener noreferrer" > Entradas </a> </li>
        <li><a className="App-link" href="#" target="_blank" rel="noopener noreferrer" > Categoria </a> </li>
        <li><a className="App-link" href="#" target="_blank" rel="noopener noreferrer" > Etiquetas </a> </li>
        <li><a className="App-link" href="#" target="_blank" rel="noopener noreferrer" > Empresa </a> </li>
        <li><a className="App-link" href="#" target="_blank" rel="noopener noreferrer" > Usuarios </a> </li>
        </ul>
      </header>
    </div>
  );
}

export default App;
